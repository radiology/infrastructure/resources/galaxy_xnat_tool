# galaxy_xnat_tool

This repository contains Galaxy tool definitions to download data from XNAT.

This is to connect the [Galaxy workflow engine](<https://docs.galaxyproject.org/en/master/index.html>) with [XNAT](https://www.xnat.org/) using the [XNATpy library](https://xnat.readthedocs.io/).

## Writing Galaxy tools

The tools follow the specification on the Galaxy team found at [Galaxy Tool XML File](https://docs.galaxyproject.org/en/master/dev/schema.html) documentation.

To build, lint and test tool the tool we use [Planemo](https://planemo.readthedocs.io)

The page [Building Galaxy Tools Using Planemo](https://planemo.readthedocs.io/en/latest/writing_standalone.html) lays out how to write tools for Galaxy using Planemo.

## XNATpy

To download data from XNATpy the `xnat download batch` CLI command is used.
